
### Pushing the boundary
This repository includes background literature and python scripts for a model from  mathematical ecology.

#### jupyter notebooks
[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/hausdorff%2FBeavers/master?filepath=python%2Fjupyter%2Fexplore_travellingwave.ipynb)
I designed a couple of jupyter notebooks, you can find them in the folder 'python/jupyter'.
The notebook 'exploring_travellingwave.ipynb'  shows preliminary simulations of the expected behaviour of solutions.
To run it interactively click on the binder button above or follow this 
<a name="link">
https://mybinder.org/v2/gl/hausdorff%2FBeavers/master?filepath=python%2Fjupyter%2Fexplore_travellingwave.ipynb
</a>


#### latex draft
I wrote a draft discussing the existing literature.
The folder '/tex' contains the files to generate the pdf-draft.
If you have the tool 'rubber' installed, you can build the document by invoking make.
Otherwise call your favorite tool (e.g. latexmk or pdflatex) on the file 'master.tex'
