""" Solves the ODE associated to travelling wave-fronts of Fisher-KPP equation

    inspired by example on https://github.com/hplgit/odespy
    
    JF on 29-09-2018
"""

def f(t,u):
    """2x2 system for a van der Pool oscillator."""
    return [u[1], 3.*(1. - u[0]*u[0])*u[1] - u[0]]

def upward_cannon(t, y): return [y[1], -0.5]

def fisher(t,u):
    """ ODE for fisher wave profile"""
    return [u[1], -1.*u[1] - u[0]*(1.-u[0])]


import numpy as np
from scipy.integrate import solve_ivp

I= [1.0, -0.1]
t_span = [0,30]
sol = solve_ivp(fisher,t_span ,I )
u = sol.y
t = sol.t

u0 = u[0,:]
from matplotlib.pyplot import *
plot(t, u0)
show()
