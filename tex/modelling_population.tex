\section{Modelling the evolution of the population density}
\label{sec:modelling_population_density}

 We  present a model for the spread of an ecosystem engineering species across the  domain $\R$
and denote by 
\begin{displaymath}
                        u : \tdomain  \times \R \longrightarrow \R_{\geq 0}
\end{displaymath}
the population density.
Starting from a random walk we arrive at a diffusive PDE.
The key idea here is that there exists a time-dependent boundary 
\begin{displaymath}
				L(t):\tdomain \longrightarrow \R
\end{displaymath}
which separates two different habitats, 
\begin{displaymath}
    \text{\textit{the wilderness} }
    E_1 := \left\{ x > L(t) \right\} 
    \quad
    \text{ and \textit{the colony} }
    E_{2} := \left\{ x < L(t) \right\} .
\end{displaymath}
 The mobility and the reproduction of the individuals differs between $E_1$ and $E_2$.

\subsection{The model}
\label{sec:modelling:density_equation:model_A}

This model is presented in the paper by \citeauthor{maciel_how_2013} (see \cite{maciel_how_2013}),
we follow their notation.
\footnote{That means the notation differs from Lutscher's original draft 'Pushing the boundaries.'}


\includegraphics[page=1,angle=-90,width=\textwidth,clip,trim= 7cm 0cm 3cm 0cm]{pictures/randomwalks.pdf}

Within the interior the individuals perform a symmetric random walk with probabilities $p_1$ respectively $p_2$. 
If $p_i <1$, individuals may also rest with probability $(1-p_i)$.
Only those individuals at the boundary behave differently . 
An individual at $L(t)$ jumps into the wilderness $E_1$ with rate $\alpha_1$  and moves into the colony with rate $\alpha_2$. 

At this stage we describe a process without births and deaths and require
\begin{math}
				0 \leq p_i \leq 1
\end{math}
and 
\begin{math}
				0 \leq \alpha_1 + \alpha_2 \leq 1 .
\end{math}

\begin{remark}[Interpretation of parameters]
    \label{rem:model_A:parameters}
    In this setting the crossing probabilities $\alpha_1$ and $\alpha_2$ measure the relative appeal of the habitats.
    Note that they are independent of the choice of $p_i$.
    The condition $\alpha_1 + \alpha_2 = 1$ is natural since this reflects the basic symmetry that if the individuals are more likely to cross from $E_1$ into $E_2$, then the reverse jump should be less attractive (i.e. $\alpha_1$ small).
\end{remark}

\subsubsection{Heat equation in the interior}
The  master equations for this stochastic process lead to a heat equation inside the two domains where the diffusion coefficient differs.

\begin{displaymath}
				u_t(t,x) = D_i u_{xx}(t,x) 
				\qquad \forall 	x \in E_i .
\end{displaymath}


For the derivation using Taylor expansions see appendix \ref{sec:appendix:density_equation:model_A}).


\subsubsection{Jump condition for the density}
As outlined in \cite{maciel_how_2013} we can derive the following expression for the boundary condition at $L(t)$:
\begin{align*}
				u(t,L(t)^-) = & 
                                k_A
				u(t,L(t)^+)  
\end{align*}
with 
\begin{displaymath}
    k_A = 
				\sqrt{\frac{d_1}{d_2}}
				\frac{\alpha_2}{\alpha_1} \frac{p_1}{p_2},
\end{displaymath}
where the parameters $d_i>0$ are related to the ratio of the grid sizes in the \emph{parabolic limit}, 
appendix \ref{sec:appendix:density_equation:model_A} gives the detailed steps of this derivation.

\subsubsection{Reaction terms}
To account for birth and deaths of the individuals we add reaction terms. 
Within the colonized environment $E_2$ the population  grows logistically up to a carrying capacity $r$.
In the wilderness $E_1$ the individuals die at a rate $m > 0$, no births are possible. 

This leads to the following system of equations
\begin{align*}
				u_t(t,x) = &
                                D_1 u_{xx}(t,x)  + u(t,x) \cdot (r - u(t,x)) 
                                & x < L(t) \\
				u_t(t,x) = &
                                D_2 u_{xx}(t,x) - m u(t,x) 
                                & x > L(t) \\
\end{align*}
coupled through the boundary condition 
\begin{align*}
				u(t,L(t)^-) = & 
                            k_A
				u(t,L(t)^+)  
                                .
\end{align*}

\subsection{Outlook}

\subsubsection{Further models}
Two further models have been derived in appendix \ref{sec:appendix:density_equation:model_B} and \ref{sec:appendix:density_equation:model_C}. 
They all share the same diffusion equation and a jump condition of the type 
\begin{align*}
				u(t,L(t)^-) = & 
                            k
				u(t,L(t)^+)  
                                ,
\end{align*}
only the value of $k$ varies.

\subsubsection{Mathematical rigour}
The derivation of the model through Taylor expansions is not entirely rigorous, see remark \ref{rem:model_A:inconsistency} in appendix \ref{sec:appendix:density_equation:model_A}.
So far the reaction terms have been introduced ad-hoc. 
In principle a derivation of the entire reaction-diffusion PDE from an extended Markov process with exponential death/birth clocks should be possible.


\subsubsection{Background on parabolic equations in biology}
The book \citetitle{perthame_parabolic_2015}  by \Textcite{perthame_parabolic_2015} gives a thorough presentation of the mathematical properties and questions associated with reaction-diffusion PDEs in biology.
In contrast to linear models (heat equation), they allow for solutions exhibiting patterns just as we observe them in nature.
A very basic pattern are travelling wave solutions, the mathematical analogue of the spread of populations across a landscape.
In the next section \ref{sec:evolution_boundary} we are going to derive an equation for $L(t)$ in order to close our system,
then a travelling wave solution is constructed in section \ref{sec:travelling_wave}.

