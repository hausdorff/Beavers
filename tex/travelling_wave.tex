\section{Travelling Wave Solution}
\label{sec:travelling_wave}

We recall our generic model as
\begin{align}
    \label{eq:rd_left}
    u_t(t,x) =& D_2 u_{xx}(t,x) + u(t,x) ( r - u(t,x))    & x < L(t) \\
    \label{eq:rd_right}
    u_t(t,x) =& D_1 u_{xx}(t,x) - m u(t,x)               & x > L(t)
\end{align}
where $r >0$ is the carrying capacity of the colonized environment and $m >0 $ the natural death rate in the wilderness.

These two reaction-diffusion equations are coupled through the \emph{jump condition} 
\begin{equation}
    u(t,L(t)^-) = k u(t,L(t)+)
    \label{eq:k_condition}
\end{equation}
for some parameter $k$ (usually $k>1$) related to the habitat preference of the species (usually for the colony $x<L(t)$).
This condition is discussed in detail in \cite{maciel_how_2013} and has been derived in a similar manner in section \ref{sec:modelling_population_density}.

Furthermore we have an equation for the boundary of the form
\begin{displaymath}
    \dot{L}(t) = G(L(t), u(t,\cdot)) .
\end{displaymath}
Here we are going to work with the \emph{flux condition} derived in detail in section \ref{sec:evolution_boundary:continuity_flux}.
Alternative choices of $G$ are discussed in section \ref{sec:evolution_boundary}.
\begin{equation}
    \dot{L} (t)\cdot
    \left[ u(t,L(t)^-) - u(t,L(t)^+) ] \right]
    =
    D_1 u_x(t, L(t)^+) - D_2 u_x(t,L(t)^-)
\end{equation}

\subsection{Constant boundary speed}
\label{sec:travelling_wave:strategy}
In the following we assume that the boundary travels at speed $ \dot{L}(t)= c > 0$
and derive a travelling wave solution
\begin{displaymath}
    u(t,x) = U(x - ct)
\end{displaymath}
for a suitable wave profile
\begin{math}
    U: \R \to \R_{\geq 0}.
\end{math}

Along with the conditions \eqref{eq:k_condition} and \eqref{eq:flux_condition} we are going to impose
\begin{align*}
    \lim_{z \rightarrow \infty} U(z) & = \lim_{x \rightarrow \infty} u(t,x)  = 0 \\
                                \lim_{z \rightarrow - \infty} U(z) & = \lim_{x \rightarrow -\infty} u(t,x)  = r
\end{align*}

This corresponds to a wave propagating to the right which connects the equilibrium $U=0$ of  equation \eqref{eq:rd_right} with the full capacity equilibrium at $U=r$ of the Fisher-KPP equation \eqref{eq:rd_left}.


\paragraph{Strategy} We to look for profiles 
\begin{math}
    V,W : \R \to \R_{\geq 0}
\end{math}
for each of the two equations \eqref{eq:rd_left} and \eqref{eq:rd_right} separately and then glue these together such that the boundary conditions are satisfied: 
\begin{displaymath}
     U(z) = 
     \begin{cases}
         W(z) & z <0 \\
         V(z) & z >0 \\
     \end{cases}
     .
\end{displaymath}
These profiles are determined by two ODEs derived from equations \eqref{eq:rd_left} and \eqref{eq:rd_right} through a change of variables 
\begin{math}
    z = x - c t
\end{math}
Dropping the argument $z$ we arrive at
\begin{align}
    W'' & = \frac{-c}{D_2} W' - \frac{1}{D_2} W (r-W) & z < 0 \label{eq:ode_left} \\
                                V'' & = \frac{-c}{D_1} V' + \frac{m}{D_1} V			  & z > 0 \label{eq:ode_right}
\end{align}
The boundary conditions \eqref{eq:k_condition} and \eqref{eq:flux_condition} then read as 
\begin{align}
    \label{eq:k_condition:for_profile}
    W(0) & = k V(0) \\
    \label{eq:flux_condition:for_profile}
    c \cdot \left[ W(0) - V(0) \right]
    & = D_1 V'(0) - D_2 W'(0).
\end{align}
where we have omitted the superscripts since we expect the solutions $V(z)$,$W(z)$ to be continuous.
First we are going to solve equation \eqref{eq:ode_right} by an exponential ansatz (section \ref{sec:travelling_wave:profile_wilderness}),
then use this explicit expression to reduce the equations \eqref{eq:k_condition:for_profile} and \eqref{eq:flux_condition:for_profile} (see section \ref{sec:travelling_wave:assembling_solution}) to  something involving only $W(z)$.
In a last step we solve for $W(z)$ in section \ref{sec:travelling_wave:profile_colony}.


\subsubsection{Wave profile for constant death rate}
\label{sec:travelling_wave:profile_wilderness}

We are now going to solve equation \eqref{eq:ode_right}.
This is a simple linear ODE.
Taking an exponential ansatz of the form
\begin{displaymath}
    V(z) = B \; \exp(bz)
\end{displaymath}
we get
\begin{displaymath}
    b^2 V(z) = -b \frac{c}{D_1} V(z) + \frac{m}{D_1} V(z) .
\end{displaymath}

Solving for the roots of the polynomial
\begin{displaymath}
    D_1 b^2 + c b - m = 0
\end{displaymath}

we get
\begin{displaymath}
    b_{\pm} = \dfrac{- c \pm \sqrt{c^2 + 4 m D_1}}
                                                                        { 2 D_1}
\end{displaymath}

Our demand that the density must vanish for $z \rightarrow \infty$ lets us exclude the case $b>0$.
Thus we have
\begin{displaymath}
    b = \dfrac{- c - \sqrt{c^2 + 4 m D_1}}
                                                                        { 2 D_1}
                                        < 0
\end{displaymath}

The amplitude $B$ will be determined through the boundary conditions for $z=0$ (equations \eqref{eq:k_condition:for_profile} and \eqref{eq:flux_condition:for_profile}).

\subsubsection{Assembling the solution}
\label{sec:travelling_wave:assembling_solution}

The explicit solution 
\begin{math}
    V(z) = B \exp(bz) 
\end{math}
 allows us to insert
 \begin{displaymath}
     V(0) = \frac{1}{k} W(0) 
    \qquad
     V'(0) = \frac{b}{k} W(0)
 \end{displaymath}
 into the flux condition \eqref{eq:flux_condition:for_solution_curve}, which reduces to
 \begin{align*}
      c \cdot \left[ W(0) - \frac{1}{k}W(0) \right]
      & = D_1 \frac{b}{k} W(0) - D_2 W'(0)  \\
     \iff \qquad \qquad \qquad
      D_2 W'(0) 
     & = W(0) \left[ \frac{1}{k} (D_1b + c) - c \right] .
 \end{align*}

This  equation  solely depends on the profile $W$.
Subsequently we are going to show  in section \ref{sec:travelling_wave:profile_colony} that there exists a solution $W$ to eq. \eqref{eq:ode_left} satisfying furthermore
\begin{math}
    \lim_{z \to - \infty} W(z) = r.
\end{math}

If in addition   we can establish the existence of a point $z^* \in \R$ such that the modified flux condition 
\begin{displaymath}
    \label{eq:flux_condition:for_solution_curve}
    W'(z^*)
     =  \frac{1}{D_2}  \left[ \frac{1}{k} (D_1b + c) - c \right]W(z^*)
     =: \gamma W(z^*)
\end{displaymath}
holds, we can choose 
\begin{displaymath}
    B := \frac{1}{k} W(z^*)
\end{displaymath}
such that the jump condition is satisfied and finally define the wave profile as
\begin{equation}
    U(z) = 
    \begin{cases}
        W(z+z^*) & z <0 \\
        \frac{1}{k} W(z^*) \exp(bz) & z>0
    \end{cases}
    \label{eq:solution_profile}
\end{equation}

\subsubsection{Wave profile for the Fisher equation}
\label{sec:travelling_wave:profile_colony}


It suffices to find a semi-orbit $W(z)$ for the Fisher equation
\begin{displaymath}
    W''  = \frac{-c}{D_2} W' - \frac{1}{D_2} W (r-W)  \label{eq:ode_left:fisher_eq}
\end{displaymath}
connecting the full capacity equilibrium $r$ with a point $z^*$ such that 
\begin{displaymath}
     W'(z^*) = \gamma W(z^*)
     \qquad \qquad
    \gamma := \frac{1}{D_2}  \left[ \frac{1}{k} (D_1b + c) - c \right]
\end{displaymath}

To this end we will follow the treatment given in\cite{kot_elements_2008}.
We introduce the notation
                                \begin{displaymath}
                                    W_1(z) := W (z), \qquad
                                                                W_2(z) := W'(z) ,
                                \end{displaymath}

This allows us to rewrite the ODE \eqref{eq:ode_left:fisher_eq} as a two-dimensional system of first order ODEs
\begin{equation}
    \begin{pmatrix}
        W_1' \\ W_2'
    \end{pmatrix}
                                =
                                \begin{pmatrix}
                                    W_2 \\ \frac{-c}{D_2} W_2 - \frac{1}{D_2} W_1 (r-W_1)
                                \end{pmatrix}
                                =: F(W_1, W_2)
                                \label{eq:first_order_system}
\end{equation}

There are two equilibrium points: $(0,0)$ and $(r,0)$.


For further stability analysis of the equilibrium points, we turn to the eigenvalues of the linear approximation.
\begin{displaymath}
    DF(0,0) =
        \begin{pmatrix}
            0 		& 1 \\
            \frac{-r}{D_2}  & \frac{-c}{D_2}
        \end{pmatrix}
    \qquad
    DF(r,0) =
        \begin{pmatrix}
            0 		& 1 \\
            \frac{r}{D_2}  & \frac{-c}{D_2}
        \end{pmatrix}
\end{displaymath}

\subparagraph{Equilibrium at $(r,0)$}
The eigenvalues of
\begin{displaymath}
    DF(r,0) =
        \begin{pmatrix}
            0 		& 1 \\
            \frac{r}{D_2}  & \frac{-c}{D_2}
        \end{pmatrix}
\end{displaymath}
are
\begin{displaymath}
    \omega_{\pm} = \dfrac{ -c \pm \sqrt{c^2 + 4D_2r}}  {2 D_2}
\end{displaymath}
with the corresponding eigenvectors
\begin{displaymath}
            \begin{pmatrix}
                1 \\ \omega_{\pm}
            \end{pmatrix}
\end{displaymath}

The eigenvalue $\omega_+ > 0$ corresponds to a repelling manifold, the eigenvalue $\omega_- < 0$ implies an attractive curve.
Hence we conclude that this equilibrium point at $(r,0)$ is a saddle node.

\subparagraph{Equilibrium at $(0,0)$}

The eigenvalues of
\begin{displaymath}
    DF(0,0) =
            \begin{pmatrix}
                0 		& 1 \\
                \frac{-r}{D_2}  & \frac{-c}{D_2}
            \end{pmatrix}
\end{displaymath}
are
\begin{displaymath}
    \nu_{\pm} = \dfrac{ - c \pm \sqrt{ c^2 - 4D_2r}} {2D_2}
\end{displaymath}

The eigenvectors are 
\begin{displaymath}
        \begin{pmatrix}
            1 \\ \nu_{\pm}
        \end{pmatrix}
\end{displaymath}

Because $\nu_{\pm} < 0$, these point into the 4th quadrant of the $W_1$-$W_2$-plane.
\\

\paragraph{Case $ c> 2 \sqrt{D_2r}$}
\label{sec:travelling_wave:profile_wilderness:case_c_big}
We have two negative eigenvalues, so the equilibrium point $(0,0)$ is a stable node.
It was shown by Fisher that there exists a heteroclinic orbit connecting the two equilibria entering at $(0,0)$ through $\nu_+$ as $z\to\infty$.
A corresponding argument taken from \cite{kot_elements_2008} is spelled out in appendix \ref{sec:appendix:heteroclinic_orbits}. 

To ensure that the modified flux condition \eqref{eq:flux_condition:for_solution_curve} holds we have to find a point $z^*$ of intersection between the solution trajectory and the straight line $W_2= \gamma W_1$.
It is sufficient to demand
\begin{equation}
    0 > \gamma > \nu_+
    \label{eq:condition_for_intersection}
\end{equation}
since then the solution curve approaching $(0,0)$ will intersect the straight line.

Inserting 
\begin{displaymath}
    \gamma = \frac{1}{D_2}  \left[ \frac{1}{k} (D_1b + c) - c \right]
\end{displaymath}
we see that the right inequality in
eq. \eqref{eq:condition_for_intersection}in particular implies
\begin{displaymath}
      \frac{1}{k} (D_1b +c)> D_2 \nu_+ +c
\end{displaymath}
We observe
\begin{align*}
    0 > D_1 b + c = \dfrac{c - \sqrt{c^2 + 4D_1m}}{2} \qquad
  D_2 \nu_+ + c = \dfrac{c + \sqrt{c^2 - 4D_2r}}{2}  >  0
\end{align*}
and arrive at 
\begin{displaymath}
   0 >   \dfrac{D_1 b +c}{D_2 \nu_+ + c} > k
\end{displaymath}
But $ k <0$ which is impossible since we cannot have negative densities satisfying the jump condition \eqref{eq:k_condition}.
We conclude that in the case $c > 2 \sqrt{D_2r}$ there is no travelling wave solution.

\paragraph{Case $ c< 2 \sqrt{D_2r}$}
\label{sec:travelling_wave:profile_wilderness:case_c_small}
At $(0,0)$ we have an attractive spiral since
\begin{math}
    \Re(\nu_{\pm}) < 0.
\end{math}
This means the solution curve intersects the lower $W_2$-axis.
Hence any straight line through the origin with negative slope will have a point of intersection.
It suffices to check 
\begin{displaymath}
    0 > \gamma 
\end{displaymath}
for some $c < 2\sqrt{D_2r}$.
We get 
\begin{displaymath}
    c > \frac{1}{k} (D_1b +c)
    \qquad 
    (k-1) c  > \underbrace{\dfrac{-c - \sqrt{c^2 + 4D_1m}}{2} }_{ < -c}
\end{displaymath}
which is satisfied for any $k >0$.


\subsection{Two-phase Stefan boundary speed}
\label{sec:travelling_wave:two_phase_speed}

To derive the travelling wave solution we assumed $\dot{L}(t)=c$. 
This assumption can be me made without loss of generality.
If we take the profile $U(z)$ constructed above as given, depending on two parameters $c$ and $k$ a-priori unrelated to the boundary speed, one can show 
\begin{displaymath}
    \dot{L}(t)   = \frac{1}{k-1} \frac{1}{u(t,L(t)^+)} 
        \left[ D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-) \right]  
\end{displaymath}
will be constant equal to $c$.

\begin{lemma}
    Let $k>1, 0 < c < 2\sqrt{D_2r}$ and 
    \begin{displaymath}
    U(z) = 
    \begin{cases}
        W(z+z^*) & z <0 \\
        \frac{1}{k} W(z^*) \exp(bz) & z>0
    \end{cases}
    \end{displaymath}
    the profile constructed above.

    Then 
    \begin{displaymath}
        L(t)= ct \qquad \qquad u(t,x) = U(x - L(t))
    \end{displaymath}
    solves 
    \begin{align*}
    u_t(t,x) =& D_2 u_{xx}(t,x) + u(t,x) ( r - u(t,x))    & x < L(t) \\
    u_t(t,x) =& D_1 u_{xx}(t,x) - m u(t,x)               & x > L(t) \\
    \end{align*}
    satisfying both the jump-condition 
    \begin{displaymath}
    u(t,L(t)^-) = k u(t,L(t)^+)                  
    \end{displaymath}
    and  the flux-condition 
    \begin{displaymath} 
    \dot{L}(t)   = \frac{1}{k-1} \frac{1}{u(t,L(t)^+)} 
        \left[ D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-) \right]  .
    \end{displaymath}
\end{lemma}
\begin{proof}
     As seen above for any $k>0$, $0 < c < \sqrt{D_2r}$ the profile $U(z)$ is well-defined.
    In particular, there exists the point of intersection $z^*$ such that 
    \begin{displaymath}
    W'(z^*) = \gamma W(z^*)
        \qquad \text{with } 
        \gamma := \frac{1}{D_2}  \left[ \frac{1}{k} (D_1b + c) - c \right] .
    \end{displaymath}
     We rewrite 
    \begin{align*}
        u(t,L(t)^-) &= U(L(t)^- - L(t))  = U(0^-)= W(z^*) \\
        u(t,L(t)^+) &= U(L(t)^+ - L(t))  = U(0^+) = \frac{1}{k}W(z^*) \\
        u_x(t,L(t)^-) &= U'(L(t)^- - L(t)) = U'(0^-) = W'(z^*) \\
        u_x(t,L(t)^-) &= U'(L(t)^+ - L(t))= U'(0^+) = \frac{b}{k}W(z^*) 
    \end{align*}
    and finally see 
    \begin{align*}
         & \frac{1}{k-1} \frac{1}{u(t,L(t)^+)} 
        \left[ D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-) \right]  \\
        & = 
         \frac{1}{k-1} \frac{k}{W(z^*)} 
        \left[ D_1 \frac{b}{k} W(z^*) - D_2 W'(z^*) \right]  \\
        & = 
         \frac{k}{k-1}  
        \left[ D_1 \frac{b}{k}  - D_2 \gamma \right]  
        = c .
    \end{align*}
    Hence by construction of $U(z)$ we have for all $t>0$ 
    \begin{displaymath}
        \dot{L}(t) = 
         \frac{1}{k-1} \frac{1}{u(t,L(t)^+)} 
        \left[ D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-) \right]  
    \end{displaymath}
     
\end{proof}

\begin{remark}
    \label{rem:equivalence_intersection_initial_boundary_speed}
     We observe that choosing $c$ such that the point of intersection $z^*$ with
        \begin{displaymath}
            W'(z^*) = \gamma W(z^*)
        \end{displaymath}
        exists is equivalent to solving
        \begin{displaymath}
            \dot{L}(0) = 
    G(u(0,L(0)^+),u_x(0,L(0)^+), u(0,L(0)^-), u_x(0,L(0)^-)) 
            \demand c .
        \end{displaymath}
\end{remark}

     
\subsection{One-phase Stefan boundary speed}
\label{sec:travelling_wave:one_phase_speed}
     Building on remark \ref{rem:equivalence_intersection_initial_boundary_speed} we can  easily generalize to other boundary speeds, e.g. the one-phase Stefan condition.

     \subsubsection{Constant latent heat}
     \label{sec:travelling_wave:one_phase_speed:constant_latent_heat}
        If we consider 
        \begin{displaymath}
            \dot{L}(t) = 
            G(u_x(t,L(t)^-) 
            = - \mu D_2 u_x(t,L(t)^-) 
        \end{displaymath}
        it suffices to find a $c>0$ and $z^* \in \R$ such that 
        \begin{displaymath}
            \dot{L}(0) \demand c 
            \qquad \iff \qquad 
            -\mu D_2 W'(z^*) = c
        \end{displaymath}
        Then $z^*$ can again be interpreted as a point of intersection between the solution curve $W(z)$ of the Fisher equation and the horizontal line 
        \begin{math}
            W'(z^*) = \frac{c}{-\mu D_2} .
        \end{math}
        From the phase diagram it becomes immediate, that such a $z^*$ exists for $c$ small enough.


    

\subsubsection{Density dependent latent heat}
\label{sec:travelling_wave:one_phase_speed:density_dependent_latent_heat}

If we choose 
\begin{displaymath}
    \mu =  \frac{1}{k-1}\frac{1}{u_x(t,L(t)^-)} 
\end{displaymath}
as contemplated in section \ref{sec:evolution_boundary:preferred_value},
the equation becomes 
\begin{displaymath}
    \dot{L}(0) \demand c 
    \qquad \iff \qquad 
    -\frac{k}{k-1}\frac{1}{W(z^*)} D_2 W'(z^*) = c ,
\end{displaymath}
we which cast again in the form as an intersection along the trajectory $W(z)$:
\begin{displaymath}
    W'(z^*) = \gamma W(z^*) 
    \qquad \text{ with } 
    \gamma = - \frac{k-1}{k} \frac{c}{D_2} .
\end{displaymath}



\paragraph{Case $ c> 2 \sqrt{D_2r}$}
As before to guarantee the point of intersection $z^*$ it is  sufficient to demand
\begin{equation}
    0 > \gamma > \nu_+
\end{equation}
since then the solution curve $(W(z),W'(z))$ approaching $(0,0)$  as $z\to \infty$ will intersect the straight line.

We see that the left inequality implies $k>1$ whereas the right inequality gives
\begin{displaymath}
      \frac{1+k}{k} c > D_2 \nu_+
\end{displaymath}
Inserting the expression for $\nu_+$ we arrive at 
\begin{displaymath}
    \frac{1}{k} > \dfrac{c + \sqrt{c^2 - 4D_2r}}{2c}
    \quad \iff \quad
    k < \dfrac{2c}{c^2 + \sqrt{c^2 - 4D_2 r}} =: \kappa(c) .
\end{displaymath}
\subparagraph{c large}
We observe $\kappa(c) > 1$ for any $c > 2 \sqrt{D_2 r}$, hence there always is a $k>1$ small enough such that the inequality holds.
Asymptotically as $c \to \infty$ we get $\kappa(c) \to 1$. 
The larger the speed the smaller the jump size $k-1$ must be. 
This counterintuitive correlation was already discussed in section \ref{sec:evolution_boundary:continuity_flux}.
\subparagraph{c small}
For $c \to 2\sqrt{D_2r}$, we get $\kappa \to 2$. 
Hence for any $1 <k <2$, there is  $c$ arbitrarily close to $2\sqrt{D_2r}$ such that a travelling wave solution exists.

\paragraph{Case $ c< 2 \sqrt{D_2r}$}
In this case any straight line through the origin with negative slope will have a point of intersection.
It suffices to check 
\begin{displaymath}
    0 > \gamma 
\end{displaymath}
for some $c < 2\sqrt{D_2r}$.
We get 
\begin{displaymath}
    0  >   - \frac{k-1}{k} 
\end{displaymath}
which is satisfied for any $k >1$, independent of $k$.


\subsection{Comparison between one-phase and two-phase Stefan boundary speeds}
\label{sec:travelling_wave:comparison_speeds}
It is interesting to note observe the different results for the two-phase Stefan boundary condition (section \ref{sec:travelling_wave:two_phase_speed}) 
\begin{displaymath}
    \dot{L}(t)   = \frac{1}{k-1} \frac{1}{u(t,L(t)^+)} 
        \left[ D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-) \right]  
\end{displaymath}

and the one-phase Stefan boundary condition (section \ref{sec:travelling_wave:one_phase_speed:density_dependent_latent_heat}.)
\begin{displaymath}
    \dot{L}(t) =
      \frac{1}{k-1}\frac{1}{u(t,L(t)^+)} 
     D_2 u_x(t,L(t)^-)
\end{displaymath}

The table lists  for a given $c>0$ the admissible choices of $k$ differ for the two boundary speeds.

\begin{figure}[h]
\begin{tabular}{ l | c | c }
    &  $0 < c < 2 \sqrt{D_2r}$ & $c > 2\sqrt{D_2r}$ \\ \hline
    one-phase Stefan boundary speed & any $k>1$ & any $1<k<\kappa(c)$ \\ \hline
    two-phase Stefan boundary speed & any $k>0$ & no solution \\
\label{table:paremeter_comparison}
\end{tabular}
    \caption{Possible choices of $k>0$ such that a travelling wave solution exits.}
\end{figure}

Most notably the two-phase Stefan boundary speed seems to allow  only for non-heteroclinic orbits $W(z)$ of the Fisher equation \eqref{eq:ode_left:fisher_eq}.
For the one-phase Stefan condition we do get a travelling wave solution even for $c>2 \sqrt{D_2r}$, in contrast to \cite[Proposition 1.2]{hadeler_stefan_2015}.
This difference stems from having $W(z^*) >0$ in our case, whereas in \cite{hadeler_stefan_2015} the author is tied to $W(z^*)=0$ because $u(t,L(t)^-)=0$.

The one-phase Stefan boundary speed enforces $k>1$, which agrees with our intuition of individuals travelling from the higher-density colony to the less populous wilderness. 
However, the two-phase Stefan boundary allows for waves travelling to the right $c<0$ even for $k<1$.
In is not clear whether a corresponding phenomenon in ecology exists.

\subsection{Previous results}
\label{sec:travelling_wave:previous_results}
The Stefan problem as a model for population spread has been propagated by a series of papers by \textcite{du_spreading-vanishing_2010},
\textcite{du_erratum:_2013} and \textcite{du_stefan_2012}.

A nice review is given in \textcite{bunting_spreading_2012}, to which we will refer here.

\subparagraph{\textcite{bunting_spreading_2012}} consider in different notation the one-phase Stefan problem
\begin{align*}
    u_t(t,x) &= D_2 u_{xx}(t,x)  + u (r-u)&  x < L(t) \\ 
     u(t,L(t)) &= 0 &\\
    \dot{L}(t) &= - \mu D_2 u_x(t,L(t)^-) &
\end{align*}
The equation for $\dot{L}(t)$ is derived somewhat ad hoc through the 'Preferred density at the boundary'-argument, 
compare \cite[section 1.3]{bunting_spreading_2012}.
They argue the population density should be kept at a level $k>0$, but in their model clearly $k=0$.
In our model  the boundary speed follows naturally by conservation of mass, i.e. we demand that the flux across $L(t)$ is conserved (see section \ref{sec:evolution_boundary:continuity_flux}).
It is the jump-condition established in \cite{maciel_how_2013} that makes the difference: 
By adding 
\begin{displaymath}
    u(t,L(t)^-) = k u(t,L(t)^+)
\end{displaymath}
to the reaction-diffusion equation we get a consistent boundary speed for free.


\subparagraph{\textcite{hadeler_stefan_2015}} constructs 
for
\begin{math}
    c \in (0, c_0) 
\end{math}
 a travelling wave solution to the one-phase Stefan problem 
\begin{align*}
u_t(t,x) &= D_2 u_{xx}(t,x) + f(u)  &  x < L(t) \\ 
 u(t,L(t)) &= 0 &\\
\dot{L}(t) &= - \mu D_2 u_x(t,L(t)^-) &
\end{align*}
for arbitrary growth function $f \in C^2[0,1]$ satisfying
\begin{displaymath}
f(0)=f(1)= 0,
\qquad
f(u) > 0 \; \forall 0<u<0,
\qquad
f'(0)f'(1) < 0 .
\end{displaymath}
Here $c_0$  is some depending on the bifurcations of the profile ODE associated  with $f$.
For $f(u) = u(r-u)$ one gets as expected 
\begin{displaymath}
    c_0 = 2 \sqrt{D_2 r} .
\end{displaymath}
The boundary speed is derived from a Cattaneo system, compare \cite[p.15]{hadeler_stefan_2015}.
A main difference is that \citeauthor{hadeler_stefan_2015} just as \citeauthor{bunting_spreading_2012} in assembling the travelling wave solution needs to find a point $z^*$ such that 
\begin{displaymath}
    W(z^*) = 0.
\end{displaymath}
In our model we are looking for a point of intersection 
\begin{displaymath}
    W'(z^*) = \gamma(k,c) W(z^*),
\end{displaymath}
where for fixed $c$ this slope of the straight line is usually a function of $k$.
This additional freedom makes finding the point of intersection much easier as we can try to adjust $k$ such that the straight line cuts through the trajectory leaving the stable node $(W(z),W'(z)) = (r,0)$.
As a consequence we are able to construct travelling wave solutions even for $c>2\sqrt{D_2r}$, see the discussion in section \ref{sec:travelling_wave:comparison_speeds}.      

\subsection{Outlook}
\label{sec:travelling_wave:outlook}
\subsubsection{Other equation for boundary spread}
As long as the equation 
\begin{math}
    \dot{L}(0) \demand c
\end{math}is linear in $V(0),V'(0),W(0),W'(0)$ we can proceed by the same strategy and reduce
\begin{displaymath}
    \dot{L}(0) = G(V(0),V'(0), W(0),W'(0)) \demand c
\end{displaymath}
to a linear equation of the form 
\begin{displaymath}
    W'(z^*) = \gamma W(z^*) .
\end{displaymath}
Here the $\gamma$ now depends on the choice of $G$.
If $W(z)$ is the solution to the Fisher equation \eqref{eq:ode_left:fisher_eq} it suffices to check for which $k,c$  we are guaranteed the point of intersection $z^*$.


\subsubsection{Allee effect for $ x < L(t)$}
Here we constructed a travelling wave solution for a population which grows logistically in  $x < L(t)$.
Results in \cite{hadeler_stefan_2015} generalize to much more general growth functions of Allee type.
In this case we have to replace the discussion in section \ref{sec:travelling_wave:profile_colony} because $\gamma <0$ might not suffice to establish the existence of the point $z^*$ of intersection.

\subsubsection{Logistic growth in $x > L(t)$}
Our construction of $U(z)$ relied on the explicit solution $V(z)$ to the ODE \eqref{eq:ode_right}.
Using exponential decay in \eqref{eq:rd_right} gave us a simple expression for 
\begin{math}
    V'(0) = b V(0)
\end{math}
which we used to solve $\dot{L}(0) = c$.
As long as a similar expression is available, replacing the exponential decay by other growth functions in \eqref{eq:rd_right} seems within reach, compare \cite[section 4.4]{perthame_parabolic_2015}.

\subsubsection{Pictures}
The paper \cite{bunting_spreading_2012} provides pictures of the travelling wave fronts for different wave speeds.
It would be interesting to see how the shape of the travelling wave profile constructed above changes with $k$ and $c$.
Drawings of the phase-diagrams such as in \cite{hadeler_stefan_2015} could help to picture the intersection point $z^*$ and see how the construction can fail for heteroclinic orbits.

\subsubsection{Spreading-vanishing dichotomy}
A key feature which have led to the recent popularity of Stefan problem in mathematical ecology is the spreading-vanishing dichotomy presented in \cite{du_spreading-vanishing_2010}.
In contrast to the Fisher-KPP equation on its own, solutions of the Stefan problem due not spread in all cases.
It would be assuring to see that this dichotomy holds for our model, too. 


