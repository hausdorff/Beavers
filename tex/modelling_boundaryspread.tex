\section{Modelling the evolution of the boundary}
\label{sec:evolution_boundary}

In the previous section we have derived two reaction-diffusion equations for the population density coupled through the jump condition 
\begin{equation}
    \label{eq:k_condition_general}
    u(t,L(t)^-) = k u(t,L(t)^+)
\end{equation}
We now turn to the evolution of $L(t)$ to complete the model.

\subsection{The Stefan problem}
\label{sec:evolution_boundary:stefan_problem}

The Stefan problem was introduced  late in the 19th century to describe the moving interface between a melting slab of ice of temperature $u=0$ and the surrounding water at positive temperature $u>0$ (\cite{vasilev_stefan_2011}).
The speed of the front between ice ($E_1$) and water ($E_2$)  depends on the heat flux from $E_2$ to $E_1$, i.e. the temperature gradient $u_x$.
\begin{define}[One-phase Stefan problem]
This leads to the \emph{one-phase Stefan problem} given by 
\begin{align}
    \nonumber
        u_t(t,x) &= D_2 u_{xx}(t,x)  &  x < L(t) \\ 
    \label{eq:one_phase_Stefan_boundary_condition}
     u(t,L(t)) &= 0 &\\
    \label{eq:one_phase_boundary_speed}
    \dot{L}(t) &= - \mu D_2 u_x(t,L(t)^-) &
\end{align}
where $\mu^{-1} >0$ is called the \emph{latent heat}.
\end{define}
\begin{define}[two-phase Stefan problem]
If there is a bidirectional exchange of heat one arrives at the \emph{two-phase Stefan model}
\begin{align}
    \nonumber
        u_t(t,x) &= D_2 u_{xx}(t,x)  &  x < L(t) \\ 
    \nonumber
        u_t(t,x) &= D_1 u_{xx}(t,x)  & x > L(t)    \\ 
    \label{eq:two_phase_Stefan_boundary_condition}
     u(t,L(t)^-) &= u(t,L(t)^+) &\\
    \label{eq:two_phase_boundary_speed}
    \dot{L}(t) &= - \mu_2 D_2 u_x(t,L(t)^-)  + \mu_1 D_1 u_x(t,L(t)^+) &
\end{align}
\end{define}

If we replace the boundary condition \eqref{eq:two_phase_Stefan_boundary_condition} by our jump condition \eqref{eq:k_condition_general}, we recover our model derived in section \ref{sec:modelling_population_density}.
In the following we present several attempts to derive an equation for $\dot{L}(t)$ similar to \eqref{eq:two_phase_boundary_speed}.

\subsection{Continuity of flux across boundary}
\label{sec:evolution_boundary:continuity_flux}

The demand that the flux of individuals across the interface is continuous is sufficient to derive an equation for $\dot{L}(t)$. 
Since the interface is moving at the speed $\dot{L}(t)$ during the time step $\Delta t$ a total number of 
\begin{displaymath}
    \dot{L} (t)\cdot
    \left[ u(t,L(t)^-) - u(t,L(t)^+)  \right]
   \Delta t 
\end{displaymath}
individuals cross from the wilderness $x>L(t)$ into the colony $x<L(t)$.
This has to coincide with the diffusive flow 
\begin{displaymath}
    [ D_1 u_x(t, L(t)^+) - D_2 u_x(t,L(t)^-) ] \Delta t,
\end{displaymath}
hence we arrive at the \emph{flux condition}
\begin{equation}
    \dot{L} (t)\cdot
    \left[ u(t,L(t)^-) - u(t,L(t)^+)  \right]
    =
    D_1 u_x(t, L(t)^+) - D_2 u_x(t,L(t)^-)
    \label{eq:flux_condition}
\end{equation}
For a formal derivation see section \ref{sec:evolution_boundary:continuity_flux:derivation} below.

\paragraph{Discussion}
\begin{enumerate}[i)]
    \item If $k = 1$, then equation \eqref{eq:flux_condition} reduces to the usual continuous flux condition 
        \begin{displaymath}
            D_2 u_x(t,L(t)^- ) = D_1 u_x(t,L(t)^+) .
        \end{displaymath}
        This makes sense insofar as $k_A= 1$ implies 
        \begin{math}
            \frac{\alpha_1}{\alpha_2} = \frac{p_1}{p_2},
        \end{math}
        i.e. the relative preference at the boundary agrees with the ratio of the diffusion probabilities and the density is continuous.
    \item If $k \neq 1$, we can rewrite 
        \begin{displaymath}
            \dot{L}(t) =  \dfrac{1}{(k-1)} \dfrac{1}{u(t,L(t)^+)} 
            \left[ 
                D_1 u_x(t, L(t)^+) - D_2 u_x(t,L(t)^-)
                \right]
        \end{displaymath}
        and recover the two-phase Stefan condition as in equation \eqref{eq:two_phase_boundary_speed}.
    \item We observe that for $k>1$ the boundary travels to the right iff
        \begin{displaymath}
            D_1 u_x(t,L(t)^+)  - D_2 u_x(t,L(t)^-) > 0 ,
        \end{displaymath}
        i.e. more individuals cross from $E_1$ into $E_2$ than the other way round.
        This contradicts our intuition that the individuals push from areas of higher population densities into the less populous environment.
    \item As $k\to\infty$, the boundary speed gets slower and slower. This  follows from demand that the large difference $k-1$ in density values to be accounted for by the diffusion across the boundary. The larger the difference $k-1$, the longer it takes to pile up a stack of height $k-1$, the slower the boundary moves.
\end{enumerate}

\subsubsection{Derivation of the flux condition}   
\label{sec:evolution_boundary:continuity_flux:derivation}
This is an attempt to derive the \emph{flux condition} presented in \eqref{eq:flux_condition}.
We start by demanding that 
\begin{displaymath}
    \dt{} 
    \int^{L(t)+\epsilon}_{L(t)-\epsilon} 
    u(t,x) \d x \    
    \demand 0
\end{displaymath}

We substitute 
\begin{math}
    x = L(t) + y
\end{math}
and pull the time-derivative into the integral 

\begin{align}
    \dt{} 
        \int_{-\epsilon}^{\epsilon} 
            u(t,L(t) + y) 
         \d{y} 
    =
    \underbrace{
        \int_{-\epsilon}^{\epsilon} 
            u_t(t,L(t) + y) 
         \d{y} 
     }_{\text{A}}
     +
     \underbrace{
        \int_{-\epsilon}^{\epsilon} 
            u_x(t,L(t) + y) \dot{L}(t)
         \d{y} 
     }_{\text{B}}
\end{align}

The latter integral B is easily resolved as 
\begin{displaymath}
    \text{B}(\e) = 
    \dot{L}(t)
    \left[ u(t,L(t)+\e) \ - u(t,L(t) - \e) \right]
    \label{eq:integral_B}
\end{displaymath}

For the first term A we split the integral into two to insert the differential equation 
\begin{displaymath}
    u_t(t,x) =
        \begin{cases}
            D_2 u_{xx}(t,x) & x < L(t) \\
            D_1 u_{xx} (t,x) & x > L(t) 
        \end{cases}
\end{displaymath}
We get 

\begin{align}
    \text{A}(\e)
    = & 
        \int_{\e}^{0} 
            u_t(t,L(t)+y) 
        \d{y}
    + 
        \int_{0}^{\e} 
            u_t(t,L(t)+y) 
        \d{y} \\
    = & 
        \int_{L(t) - \e}^{L(t)^-} 
            D_2 u_{xx}(t,x)
        \d{y}
    + 
        \int_{L(t)^+}^{L(t)+\e} 
            D_1 u_{xx}(t,x)
        \d{y} \\
    = & 
        D_2 u_x(L(t)^-) - D_2 u_x(L(t) - \e) 
    +   D_1 u_x(L(t) + \e) - D_1 u_x(L(t)^+) 
\end{align}

\paragraph{Problem}
Letting $\e \to 0$ we get $A=0$.
This occurs because we split the integral $A$ into two parts - this is not necessary for the case $D_1=D_2$ where we get 
\begin{displaymath}
    A(\e) = u_x(t,L(t)+\e) - u_x(t,L(t) -\e)
\end{displaymath}
which does not vanish as $\e \to 0$.
We expect a similar result for $D_1\neq D_2$. 
Assuming 
\begin{displaymath}
    A(0)= D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-)
\end{displaymath}
we get  $-B(0) = A(0)$, i.e. 
\begin{displaymath}
    \dot{L} (t)\cdot
    \left[ u(t,L(t)^-) - u(t,L(t)^+) ] \right]
    =
    D_1 u_x(t, L(t)^+) - D_2 u_x(t,L(t)^-)
\end{displaymath}

\paragraph{Alternative approaches}
The flux-condition is reminiscent of the Rankine-Hugoniot condition in scalar conservation laws where it is necessary to preserve the conserved quantity across a curve of discontinuity (shock curve, here $L(t)$).
Our model is in fact a conservation law with a discontinous flux function which led to splitting the integral as above.
Starting from a weak formulation of the PDE one should be able to circumvent this step and  derive the flux condition just as for general conservation laws.


\subsection{Preferred density at boundary}
\label{sec:evolution_boundary:preferred_value}

One of the few papers in mathematical biology (if not the only one) to discuss the Stefan condition is \cite{bunting_spreading_2012}
They present an argument \footnote{Regarding the origin of this idea they refer to a discussion with Chris Cosner.} 
to derive a one-phase Stefan condition  in arbitrary dimension n. 
Here we present a slightly amended version to arrive at the same two-phase Stefan condition as in the previous section \ref{sec:evolution_boundary:continuity_flux}. 

We start by recalling that our boundary condition \eqref{eq:k_condition_general} enforces that the population density to the left of the boundary $L(t)$ always stays at the \emph{preferred value} 
\begin{math}
    k u(t,L(t)^+)
\end{math}
During a small time step $\Delta t$ we assume that a total of 
\begin{displaymath}
    \label{eq:influx_boundary_region}
     \left[ D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-) \right]\Delta t 
\end{displaymath}
individuals enter the region 
\begin{math}
    [L(t),L(t + \Delta t)] .
\end{math}
Then we calculate the average density $\rho(\Delta t)$ on 
\begin{math}
    [L(t),L(t) + \Delta t]
\end{math}
as 
\begin{displaymath}
     \rho(\Delta t) 
     = 
     \dfrac{- D_2 u_x(t,L(t)^-) \Delta t} {L(t+\Delta t) - L(t)} 
     + u(t,L(t)^+)
\end{displaymath}
We demand that the average density should equal the preferred value at the boundary, 
i.e. as $\Delta t \to 0$ we should get $\rho(0) =k u(t,L(t)^+)$ and arrive at 
\begin{displaymath}
     \rho(0) 
     = 
     \dfrac{ D_1 u_x(t,L(t)^+) - D_2 u_x(t,L(t)^-) } {\dot{L}(t)} 
     + u(t,L(t)^+)
     \demand k u(t,L(t)^+)
\end{displaymath}

Thus we arrive at exactly the same condition as before: 
\begin{equation}
    \dot{L} (t)\cdot
    \left[(k-1) u(t,L(t)^+)  \right]
    =
    D_1 u_x(t, L(t)^+) - D_2 u_x(t,L(t)^-)
\end{equation}
\paragraph{Discussion}
\begin{enumerate}[i)]
\item
    In \cite{bunting_spreading_2012} the authors omit the influx from the right in  \eqref{eq:influx_boundary_region} and subsequently arrive at the one-phase Stefan condition 
        \begin{displaymath}
    \dot{L} (t)\cdot
    \left[(k-1) u(t,L(t)^+)  \right]
    =
    - D_2 u_x(t,L(t)^-) .
        \end{displaymath}
        For $k=1$, this implies that the flux across the interface vanishes completely.
        As $k\to \infty$, just as for the two-phase Stefan condition, we see the boundary speed slowing down as $k\to \infty$.
\end{enumerate}

\subsection{Thermodynamical Approaches}
\label{sec:evolution_boundary:thermodynamic}

The Stefan problem first arose in thermodynamics where strong first principles such as the conservation of mass, energy and momentum are used to derive an equation for $L(t)$.
When carrying over these equations to ecology one has give a meaning to terms such as 'specific heat capacity' for population densities.
As a curiosity we present a thermodynamical model involving a second order ODE for $L(t)$.

\paragraph{\Textcite{solomon_formulation_1985}}
Starting from the assumption that the heat flux along the temperature gradient is not immediate but delayed by some parameter $\tau$ the authors arrive at a one-phase Stefan-type problem involving  the \emph{telegraph equation}.
\begin{align*}
    \tau u_{tt} + u_t  & = D_2 u_{xx}         \qquad \qquad 0 < x < L(t) \\
     u(t,0)  & = T_0 = \mathrm{const.}                     \\
     u(t,L(t)) & = T_c = \mathrm{const.}                  
\end{align*}
where the boundary satisfies  
\begin{displaymath}
     \ddot{L}(t)  = \mu \dot{L}(t)^2 u_x(t,L(t)^-) -   \frac{1}{\tau} 
    \left[ \dot{L}(t) + \mu D_2 u_x(t,L(t)^-) \right]  .
\end{displaymath}


\paragraph{Discussion}
\begin{enumerate}[i)]
    \item 
    As $\tau \to 0$, i.e. in the limit of vanishing delay, the one-phase Stefan condition has to be satisfied.
    The authors do not provide further details on the convergence of the problem as a whole to the classical one-phase Stefan problem presented - it is a physics paper.
    \item
        In contrast to the usual heat equation the hyperbolic telegraph equation exhibits finite speed of propagation, a desirable feature in ecology models. 
        However, the mathematical treatment becomes more difficult (missing regularizing properties, maximum principles,..).
\end{enumerate}

\subsection{Probabilistic Approaches}
\label{sec:evolution_boundary:probabilistic}

In the following we present a couple of papers which derive Stefan problems rigorously from stochastic processes capturing the microscopic behaviour.
These results tend to be rather technical making use of generators, ergodic theorems and other abstract machinery of Markov processes.


\paragraph{\textcite{funaki_free_1999}}

Funaki manages to derive first the one-phase and then the two-phase Stefan problem from a microscopic description of melting.
He considers an exclusion process on $\mathbb{Z}^d$ described as follows:
\newline\newline
\begin{it}
Each site is assigned a state  in ${1,0,-1,…,-l}$: state $1$ means a water particle is present, state $0$ tells us that the site is vacant and the negative states correspond to different ice particles being present. 
At first only the water particles are allowed to move: they can either occupy a vacant site or hit an ice particle.
In the latter case the water particle disappears and the state of the ice particle decreases by one.
Hence an ice particle at state $-2$ has been hit once. twice at $-3$ and so on. 
If an ice particle in state $-l$ is hit, it disappears and the site becomes vacant. 
Hence the parameter $l$ measures the number an ice particle has to be hit by a water particle before it disappears.
\end{it}
\newline\newline
In the limit of a large number of particles $N \to \infty$ and vanishing grid size $\Delta x$ the empirical distribution of states converges to the weak solution of the following one-phase Stefan problem:
\begin{align}
    \nonumber
        u_t(t,x) &= D_2 u_{xx}(t,x)  &  x < L(t) \\ 
    \nonumber
     u(t,L(t)^-) &= 0 &\\
    \nonumber
    \dot{L}(t) &= - \frac{1}{l} D_2 u_x(t,L(t)^-) &
    .
\end{align}

If furthermore ice particles are allowed to move, too, we arrive at the two-phase Stefan problem:
\begin{align}
    \nonumber
        u_t(t,x) &= D_2 u_{xx}(t,x)  &  x < L(t) \\ 
        \nonumber
        u_t(t,x) &= D_1 u_{xx}(t,x)  &  x > L(t) \\ 
    \nonumber
     u(t,L(t)^-) &= u(t,L(t)^+) =  0 &\\
    \nonumber
     (l-1) \dot{L}(t) &=  
    - \mu_2 D_2 u_x(t,L(t)^-) 
    + \mu_1 D_1 u_x(t,L(t)^+) 
    . &
\end{align}

\subparagraph{Discussion}
In the context of ecology it is not clear how to translate the negative state values into positive population densities.
Maybe this model is more apt to derive the spread of a \emph{degree of civilization} (see appendix \ref{sec:appendix:degree_civilization}).


\paragraph{\Textcite{landim_microscopic_2006}}
Starting from a similar stochastic process  the authors recover a two-phase Stefan problem under a \emph{hydrodynamic scaling}.
The stochastic process is described as follows:
\newline\newline
\begin{it}
“Consider the one-dimensional lattice $\Z$ with each site being occupied by a molecular agglomerate of type -1 for the material in the solid state and of type 1 for the material in the liquid state. According to its internal energy, each agglomerate is classified by a heat
unit of 0 or 1. 
An interaction between neighboring sites occurs independently in the following way: If the particles are of the same type, then their heat units are interchanged after a mean a -1 exponential time for particles type -1 and after a mean a 1 exponential time for particles of type 1. 
If the particles are of distinct
type and their heat units are also distinct, at rate 1 the heat unit of the agglomerate whose heat value was 1 drops to 0 and simultaneously the other agglomerate changes type.
If the particles are of distinct type and their heat units are equal to 1,
both heat units drop to 0 after a mean 1 exponential time.
Moreover, we start with configurations such that the agglomerates are of type -1 if they are at the left of the origin; otherwise they are of type 1.”
\end{it}
\newline\newline
The authors deduce that the empirical distribution converges to an absolutely continuous measure whose density is a weak solution of the two phase Stefan problem with boundary speed 
\begin{align*}
        u_t(t,x) &= D_2 u_{xx}(t,x)  &  x < L(t) \\ 
        u_t(t,x) &= D_1 u_{xx}(t,x)  & x > L(t)    \\ 
    \dot{L}(t) &=  \mu_2 \left[ D_2 u_x(t,L(t)^-)  -  D_1 u_x(t,L(t)^+) \right] &
\end{align*}

\subparagraph{Discussion}
\begin{enumerate}[i)]
    \item The derivation is mathematically rigorous, the authors are driven by their interest in the mathematical techniques - the proof provides little intuition.
    \item If one applies this process to model population dynamics, one needs to come up with a different picture - heat transfer should be replaced by movement or so. This might not be straightforward.
\end{enumerate}


\paragraph{\Textcite{gravner_internal_2000}}
The authors use a stochastic growth model on the lattice to derive a one-phase Stefan equation.
Quoting from the abstract of the publication, the process can be pictured the following way.
\newline

\begin{it}
    “Generalized internal diffusion limited aggregation is a stochastic
growth model on the lattice in which a finite number of sites act as Poisson
sources of particles which then perform symmetric random walks with an
attractive zero-range interaction until they reach the first site which has
been visited by fewer than $\alpha$ particles, at which point they stop. Sites on
which particles are frozen constitute the occupied set.“
\end{it}
\newline

The authors show with the so-called $H_{-1}$-method that the empirical converges weakly in probability to the weak solution of the one-phase Stefan problem 
\begin{align*}
    u_t & = \Delta \gamma(u) + \sum_i c_i \delta_{x_i}       \qquad \qquad x < L(t) \\
    u(t,L(t)) &= 0              \\
    \dot{L}(t) & = - \frac{1}{\alpha} \gamma'(0) u_x(,L(t)^-) 
\end{align*}

Here the term involving $c_i$ corresponds to the particle sources.

\subparagraph{Discussion}
\begin{enumerate}[i)]
    \item The paper is not very inviting to read. Even the formulation of the Stefan condition is obfuscated since the use the inverse $s(x):=L^{-1}(x)$.
    \item This model seems to have been treated in one-dimension at an earlier stage, the authors refer to \cite{chayes_hydrodynamic_1996}. 
        For our purposes this should be enough.
\end{enumerate}


\subsection{Outlook}
\label{sec:evolution_boundary:outlook}

In the following we will stick to the two-phase Stefan condition derived both through demanding the flux to be continuous across the boundary (see section \ref{sec:evolution_boundary:continuity_flux}) and through the \emph{preferred value argument} taken from \cite{bunting_spreading_2012} (see section \ref{sec:evolution_boundary:preferred_value}).
\begin{equation}
    \dot{L} (t)\cdot
    \left[ u(t,L(t)^-) - u(t,L(t)^+)  \right]
    =
    D_1 u_x(t, L(t)^+) - D_2 u_x(t,L(t)^-)
\end{equation}
In the long run a derivation through the probabilistic approaches exposed in section \ref{sec:evolution_boundary:probabilistic} might provide both a microscopic picture  and mathematical rigour. 
Reading  the probabilistic papers one has the feeling that under the title \emph{"Hydrodynamic scaling limits of interacting particle systems"} there is a whole section of papers deriving Stefan problems from stochastic processes. 
The references in \cite{funaki_free_1999}, \cite{landim_microscopic_2006} and \cite{gravner_internal_2000} could be the starting point for further exploration, especially \cite{chayes_hydrodynamic_1996} seems promising. 
These papers tend to be very technical and in any case require suitable re-interpretation of the underlying processes in terms of individuals travelling across the landscape.
