\addcontentsline{toc}{section}{Appendix} % Eintrag in das Inhaltsverzeichnis
\section{Model A}
\label{sec:appendix:density_equation:model_A}

The first model is the one presented in the paper by \citeauthor{maciel_how_2013} (see \cite{maciel_how_2013} from \citeyear{maciel_how_2013}.
The notation follows this paper.
\meta{That means the notation differs from Lutscher's original draft 'Pushing the boundaries.'}


\includegraphics[page=1,angle=-90,width=\textwidth,clip,trim= 7cm 0cm 3cm 0cm]{pictures/randomwalks.pdf}


There are five master equations for the random walk.
\begin{align}
				\label{eq:A_2_inside}
				\begin{split}
				u(t+\D t, L(t) - 2 \D x_2) \D x_2  = &
				\frac{p_2}{2} u(t, L(t) - 3 \D x_2) \D x_2   \\ +& 
				 (1 - p_2) u(t,L(t) - 2 \D x_2) \D x_2   \\  + & 
				 \frac{p_2}{2} u(t, L(t) -  \D x_2) \D x_2  
				\end{split} \\
				\label{eq:A_2_boundary}
				\begin{split}
				u(t+\D t, L(t) -  \D x_2) \D x_2 = &
				\frac{p_2}{2} u(t, L(t) - 2 \D x_2) \D x_2  \\  + &
				(1 - p_2) u(t,L(t) -  \D x_2) \D x_2  \\  + &
				\alpha_2 u(t,L(t)) \D x_0  
				\end{split} \\
				\label{eq:A_boundary}
				\begin{split}
				u(t+\D t, L(t) ) \D x_0  = &
				\frac{p_2}{2} u(t, L(t) -  \D x_2) \D x_2  \\ + & 
				(1 - \alpha_2 - \alpha_1) u(t,L(t) ) \D x_0  \\ + &
				\frac{p_2}{2} u(t, L(t) +  \D x_2) \D x_2  
				\end{split} \\
				\label{eq:A_1_boundary}
				\begin{split}
				u(t+\D t, L(t) +  \D x_1) \D x_1  = &
				\alpha_1 u(t,L(t)) \D x_0  \\ + &
				(1 - p_1) u(t,L(t) +  \D x_1) \D x_1  \\ +& 
				\frac{p_1}{2} u(t, L(t) + 2 \D x_1) \D x_1  
				\end{split} \\
				\label{eq:A_1_inside}
				\begin{split}
				u(t+\D t, L(t) + 2 \D x_1) \D x_1  = &
				\frac{p_1}{2} u(t, L(t) +  \D x_1) \D x_1  \\ + &
				(1 - p_1) u(t,L(t) + 2 \D x_1) \D x_1  \\ + &
				\frac{p_1}{2} u(t, L(t) + 3 \D x_1) \D x_1 
				\end{split} 
				,
\end{align}
where we defined
				\begin{math}
								\D x_0 :=
								\frac{\D x_1}{2} +
								\frac{\D x_2}{2} 
								.
				\end{math}

As further condition we have 
\begin{math}
				0 \leq p_i \leq 1
\end{math}
and 
\begin{math}
				0 \leq \alpha_1 + \alpha_2 \leq 1 .
\end{math}



\subsection*{PDE in the inside}
The master equations \eqref{eq:A_2_inside} and \eqref{eq:A_1_inside} lead to the heat equation for the inside of the domains:
\begin{displaymath}
				u_t(t,x) = D_i u_{xx}(t,x) 
				\qquad \forall 	x \in E_i .
\end{displaymath}

The derivation is identical for all three models and follows the classic approach: A taylor expansion of $u$ wrt. to the spatial variable $x$ up to the second order allows to cancel various terms, the parabolic imit leads to a PDE.

\begin{define}
				As the parabolic limit we define the simultaneous limit 
				\begin{math}
								\D x_i \longrightarrow 0
				\end{math}
				and
				\begin{math}
								\D t \longrightarrow 0
				\end{math}
				such that 
				\begin{displaymath}
								d_i := \lim_{\D {x_i},\D t \rightarrow 0 }
												\dfrac{\D {x_i}^2}{2\D t}
				\end{displaymath}
				exists.
				Instead of 
				\begin{math}
								\lim_{\D {x_i},\D t \rightarrow 0 }
				\end{math}
				we just write 
				\begin{math}
								\limpara
				\end{math}
				The diffusion coefficient is defined by 
				\begin{displaymath}
								D_i := p_i d_i
				\end{displaymath}
\end{define}
\todo[Background on parabolic limit]{How do we make this parabolic limit precise ? Is there a canonical notation ? Maybe look for the common use in an introductory book on kinetic theory ?}




\subsection*{Boundary conditions for the density}
As outlined in \cite{maciel_how_2013} we can derive the expression for the boundary condition at $L(t)$.
Now we give the detailed steps of this derivation.
We use equation \eqref{eq:A_2_boundary} and \eqref{eq:A_1_boundary}.
First we perform a zeroth order taylor expansion wrt. to $\D t$ on the left-hand-side of each equation, on the right-hand side we use 
\begin{displaymath}
				u(t,L(t)\pm2\D x_i) =
				u(t,L(t)\pm \D x_i) + \O{\D x_i}
\end{displaymath}
so we get for equation \eqref{eq:A_2_boundary} 
\begin{align}
				\label{eq:A_2_boundary:simplified}
				\begin{split}
				u(t,L(t) - \D x_2) = &
				\frac{p_2}{2} u(t,L(t)-\D x_2) +
				\O{\D t}  +
				\O{\D x_2} \\ & +
				(1 - p_2) u(t,L(t) -\D x_2) +
				\alpha_2 \frac{\D x_0}{\D x_2} u(t,L(t))  \\
				= &
				(1 - \frac{p_2}{2}) u(t,L(t) -\D x_2) +
				\alpha_2 \frac{\D x_0}{\D x_2} u(t,L(t)) \\ & +
				\O{\D x_2} +
				\O{\D t} 
				\end{split} \notag \\
				\begin{split}
				\iff				
				 \frac{p_2}{2} u(t,L(t) -\D x_2) = &
				\alpha_2 \frac{\D x_0}{\D x_2} u(t,L(t)) +
				\O{\D x_2} +
				\O{\D t}
				\end{split} 
\end{align}
Similarily for equation \eqref{eq:A_1_boundary} we get
\begin{align*}
				&  \frac{p_1}{2} u(t,L(t) +\D x_1) = 
				\alpha_1 \frac{\D x_0}{\D x_1} u(t,L(t)) +
				\O{\D x_1} +
				\O{\D t}  \\
				\iff & 
				u(t,L(t)) = 
				\frac{1}{\alpha_1} \frac{\D x_1}{\D x_0} \frac{p_1}{2} u(t,L(t) +\D x_1)  +
				\O{\D x_1} +
				\O{\D t} 
\end{align*}

Inserting this into the equation \eqref{eq:A_2_boundary:simplified} to replace $u(t,L(t))$, we get
\begin{displaymath}
				 \frac{p_2}{2} u(t,L(t) -\D x_2) = 
				\frac{\alpha_2}{\alpha_1} \frac{\D x_1}{\D x_2} \frac{p_1}{2} u(t,L(t) +\D x_1)  +
				\O{\D x_1} +
				\O{\D x_2} +
				\O{\D t} 
\end{displaymath}

Taking the parabolic limit we get the boundary condition
\begin{displaymath}
				u(t,L(t)^-) = 
				\limpara \left(\frac{\D x_1}{\D x_2}\right)
				\frac{\alpha_2}{\alpha_1} \frac{p_1}{p_2} u(t,L(t)^+)  
\end{displaymath}
Using the expression 
\begin{align*}
				&
				\dfrac{\D x_1^2}{2d_1} =
				\D t =
				\dfrac{\D x_2^2}{2d_2} 
				\implies 
				\dfrac{\D x_1}{\D x_2} = 
				\sqrt{\frac{d_1}{d_2}}
\end{align*}
for the simultaneous parabolic limit we have 
\begin{displaymath}
				\limpara \left(\frac{\D x_1}{\D x_2}\right) =
				\sqrt{\frac{d_1}{d_2}}
				\implies
				u(t,L(t)^-) = 
				\underbrace{
								\sqrt{\frac{d_1}{d_2}}
								\frac{\alpha_2}{\alpha_1} \frac{p_1}{p_2}
				}_{:= k_A} 
				u(t,L(t)^+)  
\end{displaymath}
\begin{remark}
				If $\D x_1 = \D x_2$ then we have 
				\begin{math}
								k_A = 
								\frac{\alpha_2}{\alpha_1} \frac{p_1}{p_2}
				\end{math}
\end{remark}

\begin{remark}[Inconsistency]
				\label{rem:model_A:inconsistency}
				For fixed $t$,$u(t,x)$ is in general discontinous across $L(t)$, i.e. 
				\begin{math}
								u(t,L(t)^-) \neq u(t,L(t)^+).
				\end{math}
				But in our derivation we have generously used the expression $u(t,L(t))$, 
				assuming that there is a \emph{unique} value for the density at $L(t)$.
\end{remark}

\subsection*{Boundary conditions for the derivatives}
We demand that by crossing the boundaries the entities are conserved, i.e. the flux is continous at $L(t)$: 
\begin{displaymath}
				D_1 u_x(t,L(t)^-) =
				D_2 u_x(t,L(t)^+) 
\end{displaymath}
\todo[Background]{Warum ist der Fluss nochmal gerade durch $D_1 u_x$ gegeben ? Ein bisschen mehr begründen. Was haben den Lutscher und Maciel in \cite{maciel_how_2013} geschrieben ? Siehe Notizen in grünem Heft.}


\subsection*{The system}
The model A leads to the following system of equations
\begin{align*}
				u_t(t,x) = &
				D_1 u_{xx}(t,x)  
				\qquad x < L(t) \\
				u_t(t,x) = &
				D_2 u_{xx}(t,x)  
				\qquad x > L(t) \\
\end{align*}
with the boundary condition 
\begin{align*}
				u(t,L(t)^-) = & 
				\sqrt{\frac{d_1}{d_2}}
				\frac{\alpha_2}{\alpha_1} \frac{p_1}{p_2}
				u(t,L(t)^+)   \\
				D_1 u_x(t,L(t)^-) = &
				D_2 u_x(t,L(t)^+) 
\end{align*}

\clearpage
\section{Model B}
\label{sec:appendix:density_equation:model_B}

The model A presented in section \ref{sec:appendix:density_equation:model_A} leads to an inconsistency described in the remark on page \pageref{rem:model_A:inconsistency}.
This problem arises because the boundary $L(t)$ features as an explicit grid point in the discrete model.
In this model we will assume that $L(t)$ is in the middle between the last grid point in the colonized environment $E_2$ and the first grid point of the wilderness $E_1$.
Nonetheless, there is a  price to pay:  the crossing probabilites - here called $q_i$ instead of $\alpha_i$ - now depend on the diffusion parameters $p_i$.
Further on we will introduce a generalized model C in order to get rid of this limitation.

\todo[Bild einfügen]{Zeichnung des Zahlenstrahls}\done
\includegraphics[page=2,angle=-90,width=\textwidth,clip,trim= 7cm 0cm 3cm 0cm]{pictures/randomwalks.pdf}


The master equations look as follows:
\begin{align}
				\label{eq:B_2_inside}
				\begin{split}
				u(t+\D t, L(t) - 3 \frac{\D x_2}{2}) \D x_2  = &
				\frac{p_2}{2} u(t, L(t) - 5\frac{\D x_2}{2}) \D x_2   \\ + &
				(1 - p_2) u(t,L(t) - 3 \frac{\D x_2}{2}) \D x_2 \\  +&
				\frac{p_2}{2} u(t, L(t) -  \frac{\D x_2}{2}) \D x_2  
				\end{split} \\
				\label{eq:B_2_boundary}
				\begin{split}
				u(t+\D t, L(t) -  \frac{\D x_2}{2}) \D x_2  = &
				\frac{p_2}{2} u(t, L(t) - 3 \frac{\D x_2}{2}) \D x_2 \\  +&
				(1 - \frac{p_2}{2} - q_2) u(t,L(t) - \frac{\D x_2}{2}) \D x_2 \\  +&
				q_1 u(t,L(t)+ \frac{\D x_1}{2}) \D x_1  
				\end{split} \\
				\label{eq:B_1_boundary}
				\begin{split}
				u(t+\D t, L(t) +  \frac{\D x_1}{2}) \D x_1  = &
				q_2 u(t,L(t) - \frac{\D x_2}{2}) \D x_2 \\  +&
				(1 - q_1 - \frac{p_1}{2}) u(t,L(t) + \frac{\D x_1}{2} ) \D x_1 \\  +&
				\frac{p_1}{2} u(t, L(t) + 3 \frac{\D x_1}{2}) \D x_1  
				\end{split} \\
				\label{eq:B_1_inside}
				\begin{split}
				u(t+\D t, L(t) + 3 \frac{\D x_1}{2}) \D x_1  = &
				\frac{p_1}{2} u(t, L(t) +\frac{\D x_1}{2}  ) \D x_1 \\  +&
				(1 - p_1) u(t,L(t) + 3 \frac{\D x_1}{2}) \D x_1 \\  +&
				\frac{p_1}{2} u(t, L(t) + 5\frac{\D x_1}{2}) \D x_1  
				\end{split} 
\end{align}

Note that we have the conditions 
\begin{math}
				0 \leq p_i \leq 1 ,
				0 \leq q_i \leq 1 ,
\end{math}
and 
\begin{math}
				0 \leq \frac{p_i}{2} + q_i\leq 1 .
\end{math}
\begin{remark}[Interpretation of parameters]
				\label{rem:model_B:parameters}
				In contrast to model A, there is no condition relating $q_1$ and $q_2$.
				They can be chosen independently, thus enabling the puzzling combination that the crossing probabilities for a jump from the colony $E_2$ into the wilderness $E_2$ and vice versa can be large (i.e. close to $1$, depending on the diffusion parameter) at the same time.
				This is not the behaviour encountered in ecology, where intelligent beavers should prefer the colony over the wilderness independent  from which side of the boundary they are looking at it.
				A possible application of B might be a species which crosses from $E_1$ to $E_2$ along a different route (with independent probabilities) than from $E_2$ into $E_1$. 
				Think of one-way tunnels of different length in an ant colony.
\end{remark}

\subsection*{PDE in the inside}
The master equations \eqref{eq:B_1_inside} and \eqref{eq:B_2_inside} correspond to classic random walks which in the parabolic limit each lead to a heat equation 
\begin{displaymath}
				u_t = D_i u_{xx} 
				\qquad \forall x \in E_i
\end{displaymath}
where 
\begin{math}
				D_i := p_i d_i
\end{math}
just as before.

\subsection*{Boundary conditions for the density}
These two PDEs are going to be coupled through a boundary condition at $L(t)$.
To relate the density left and right of the boundary we are going to use the equations \eqref{eq:B_1_boundary} and \eqref{eq:B_2_boundary}.
We are looking for an expression of the form 
\begin{displaymath}
				u(t,L(t)^-) = k_B u(t,L(t)^+)
				.
\end{displaymath}

Equation \eqref{eq:B_1_boundary} gives us
\begin{align*}
				 u(t+\D t, L(t) -  \frac{\D x_2}{2}) = &
				\frac{p_2}{2}  u(t, L(t) - 3 \frac{\D x_2}{2})  + 
				(1 - \frac{p_2}{2} - q_1)  u(t,L(t) - \frac{\D x_2}{2}) \\ + &
				q_2 \frac{\D x_1}{\D x_2} u(t,L(t)+ \frac{\D x_1}{2}) \\
\end{align*}

Using a spatial taylor expansion around $L(t) - \frac{\D x_2}{2}$ of 
\begin{math}
				u(t, L(t) - 3 \frac{\D x_2}{2})
\end{math}
we get
\begin{align*}
				 u(t+\D t, L(t) -  \frac{\D x_2}{2}) = &
				\frac{p_2}{2}  u(t, L(t) -  \frac{\D x_2}{2}) +
				\O{\D x_2}  +
				(1 - \frac{p_2}{2} - q_1)  u(t,L(t) - \frac{\D x_2}{2}) \\ + &
				q_2 \frac{\D x_1}{\D x_2} u(t,L(t)+ \frac{\D x_1}{2})\\   
				=&
				(1 -  q_1)  u(t,L(t) - \frac{\D x_2}{2}) +
				\O{\D x_2} +
				q_2 \frac{\D x_1}{\D x_2} u(t,L(t)+ \frac{\D x_1}{2}) 
\end{align*}

Expanding 
\begin{math}
				u(t + \D t, L(t) - \frac{\D x_2}{2})
\end{math}
wrt. $\D t$ and isolating the terms involving 
\begin{math}
				u(t,L(t) - \frac{\D x_2}{2})
\end{math}
on the left hand side we get 
\begin{align*}
				q_1 u(t,L(t) - \frac{\D x_2}{2}) = &
				q_2 \frac{\D x_1}{\D x_2} u(t,L(t)+ \frac{\D x_1}{2})
				\O{\D x_2} +
				\O{\D t} 
\end{align*}

Now we take the simultaneous parabolic limit on both sides and get 
\begin{align*}
				q_1 u(t,L(t)^- ) = &
				q_2 \limpara \left(\frac{\D x_1}{\D x_2}\right) 
				u(t,L(t)^+ ) 
\end{align*}
As before we use the expression 
\begin{align*}
				&
				\dfrac{\D x_1^2}{2d_1} =
				\D t =
				\dfrac{\D x_2^2}{2d_2} 
				\qquad 
				\implies 
				\dfrac{\D x_1}{\D x_2} = 
				\sqrt{\frac{d_1}{d_2}}
\end{align*}
and get 
\begin{displaymath}
				u(t,L(t)^-) = 
				\underbrace{
								\sqrt{\frac{d_1}{d_2}}
								 \frac{q_2}{q_1}
				}_{:= k_B} 
				u(t,L(t)^+)  
\end{displaymath}
\begin{remark}
				If $\D x_1 = \D x_2$ then we have 
				\begin{math}
								k_B = 
								 \frac{q_2}{q_1}
				\end{math}
\end{remark}

\begin{remark}[Connection between Model A and Model B]
				Choose $q_1 := p_1 \beta_1$ to connect Model B to Model A.
\end{remark}



\subsection*{Boundary conditions for the derivatives}
We demand that by crossing the boundaries the entities are conserved, i.e. the flux is continous at $L(t)$: 
\begin{displaymath}
				D_1 u_x(t,L(t)^-) =
				D_2 u_x(t,L(t)^+) 
\end{displaymath}
\todo[Background]{Warum ist der Fluss nochmal gerade durch $D_1 u_x$ gegeben ? Ein bisschen mehr begründen. Was haben den Lutscher und Maciel in \cite{maciel_how_2013} geschrieben ?}

\subsection*{The system}
The model B leads to the following system of equations
\begin{align*}
				u_t(t,x) = &
				D_1 u_{xx}(t,x)  
				\qquad x < L(t) \\
				u_t(t,x) = &
				D_2 u_{xx}(t,x)  
				\qquad x > L(t) \\
\end{align*}
with the boundary condition 
\begin{align*}
				u(t,L(t)^-) = & 
				\sqrt{\frac{d_1}{d_2}}
				 \frac{q_2}{q_1}
				u(t,L(t)^+)   \\
				D_1 u_x(t,L(t)^-) = &
				D_2 u_x(t,L(t)^+) 
\end{align*}
\clearpage
\section{Model C}
\label{sec:appendix:density_equation:model_C}

This is a hybrid model between Model A described in \ref{sec:appendix:density_equation:model_A} and Model B presented in \ref{sec:appendix:density_equation:model_B}

\includegraphics[page=3,angle=-90,width=\textwidth,clip,trim= 7cm 0cm 3cm 0cm]{pictures/randomwalks.pdf}

Where as in Model B we have 
\begin{displaymath}
				q_i + \frac{p_i}{2} \leq 1
\end{displaymath}
here we have 
\begin{displaymath}
				r_i + s_i \leq 1,
\end{displaymath}
completely independent of 
\begin{math}
				p_i .
\end{math}
Of course, Model B is a special case of Model C for 
\begin{math}
				s_i = \frac{p_i}{2} .
\end{math}


The six master equations look as follows.
\begin{align}
				\label{eq:C_2_inside}
				\begin{split}
				u(t+\D t, L(t) - 5 \frac{\D x_2}{2}) \D x_2  = &
				\frac{p_2}{2} u(t, L(t) - 7\frac{\D x_2}{2}) \D x_2   \\ +   &
				(1 - p_2) u(t,L(t) - 5 \frac{\D x_2}{2}) \D x_2  \\ + &
				\frac{p_2}{2} u(t, L(t) - 3 \frac{\D x_2}{2}) \D x_2  
				\end{split} \\
				\label{eq:C_2_middle}
				\begin{split}
				u(t+\D t, L(t) - 3 \frac{\D x_2}{2}) \D x_2  = &
				\frac{p_2}{2} u(t, L(t) - 5 \frac{\D x_2}{2}) \D x_2  \\ + &
				(1 - p_2) u(t,L(t) - 3\frac{\D x_2}{2}) \D x_2  \\ + &
				s_1 u(t,L(t) - \frac{x_2}{2}) \D x_2  
				\end{split} \\
				\label{eq:C_2_boundary}
				\begin{split}
				u(t+\D t, L(t) -  \frac{\D x_2}{2}) \D x_2  = &
				\frac{p_2}{2} u(t, L(t) - 3 \frac{\D x_2}{2}) \D x_2  \\ + &
				(1 - s_1 - r_1) u(t,L(t) - \frac{\D x_2}{2}) \D x_2  \\ + &
				r_2 u(t,L(t) + \frac{x_1}{2}) \D x_1  
				\end{split} \\
				\label{eq:C_1_boundary}
				\begin{split}
				u(t+\D t, L(t) +  \frac{\D x_1}{2}) \D x_1  = &
				r_1 u(t,L(t) - \frac{\D x_2}{2}) \D x_2  \\ + &
				(1 - r_2 - s_2) u(t,L(t) + \frac{\D x_1}{2} ) \D x_1  \\ + &
				\frac{p_1}{2} u(t, L(t) + 3 \frac{\D x_1}{2}) \D x_1  
				\end{split} \\
				\label{eq:C_1_middle}
				\begin{split}
				u(t+\D t, L(t) + 3 \frac{\D x_1}{2}) \D x_1  = &
				s_2 u(t,L(t) + \frac{\D x_1}{2}) \D x_1  \\ + &
				(1 - p_1) u(t,L(t) + 3 \frac{\D x_1}{2} ) \D x_1  \\ + &
				\frac{p_1}{2} u(t, L(t) + 5 \frac{\D x_1}{2}) \D x_1  
				\end{split} \\
				\label{eq:C_1_inside}
				\begin{split}
				u(t+\D t, L(t) + 5 \frac{\D x_1}{2}) \D x_1  = &
				\frac{p_1}{2} u(t, L(t) + 3 \frac{\D x_1}{2}  ) \D x_1  \\ + &
				(1 - p_1) u(t,L(t) + 5 \frac{\D x_1}{2}) \D x_1  \\ + &
				\frac{p_1}{2} u(t, L(t) + 7\frac{\D x_1}{2}) \D x_1  
				\end{split} 
\end{align}
with the additionl conditions 
\begin{math}
				0 \leq r_i,s_i,p_i \leq 1
\end{math}
and 
\begin{math}
				0 \leq r_i + s_i \leq 1
				.
\end{math}
\todo[Check references]{Durch die Umbennung von $x_1$ zu $x_2$ und so weiter müssen auch die Verweise innerhalb der Form \\eqref{eq:\_C1\_inside} etc. angepasst werden.}

\begin{remark}
As indicated before we want to interprete the parameters $r_i$ as a measure of the relative attractivity of crossing from one habitat into another.
Here we can choose $r_i$ independent of $p_i$, just as in Model A (see remark on page \pageref{rem:model_A:parameters}) and furthermore $r_1$ and $r_2$ are not related either.
\end{remark}
\todo[Develop]{Wenn wir in Model C eine Annahme hinzufügen, um die symmetrische Attraktivität zu bekommen, dann können wir das ja auch in Model B machen...}


\subsection*{PDE in the inside}
Just as for the previous models the master equations \eqref{eq:C_2_inside} and \eqref{eq:C_1_inside} can be used do derive the heat equation  
\begin{displaymath}
				u_t = D_i u_{xx} 
				\qquad \forall x \in E_i
\end{displaymath}
where 
\begin{math}
				D_i := p_i d_i
\end{math}
just as before.

\subsection*{Boundary conditions for the density}
To derive a boundary condition for the density we use the equations \eqref{eq:C_2_boundary} and \eqref{eq:C_1_boundary}.
Again we use a zeroth order taylor expansion around $\D t$ on the left hand side of each equation and on the right hand side we insert 
\begin{displaymath}
				u(t,L(t) \pm 3 \frac{\D x_i}{2} ) =
				u(t,L(t) \pm \frac{\D x_i}{2}) 
				+ \O{\D x_i}
\end{displaymath}
to get for equation \eqref{eq:C_2_boundary}
\begin{align*}
				u(t, L(t) -  \frac{\D x_2}{2}) = & 
				\frac{p_2}{2} u(t, L(t) -  \frac{\D x_2}{2})   +
				\O{\D x_2} \\  + 
				& (1 - s_1 - r_1)  u(t,L(t) - \frac{\D x_2}{2}) & \\ 
 + &	r_2  \frac{\D x_1}{\D x_2} u (t,L(t) + \frac{\D x_1}{2})   + 
				\O{\D t} \\
				\iff
				\left( -\frac{p_2}{2} + s_1 + r_1 \right)
				 u(t t, L(t) -  \frac{\D x_2}{2}) = & 
				r_2  \frac{\D x_1}{\D x_2} u(t,L(t) + \frac{\D x_1}{2}) +
				\O{\D x_2} +
				\O{\D t} \\
\end{align*}

Taking the simultaneous parabolic limit on both sides  we are left with 
\begin{displaymath}
				u(t,L(t)^-) = 
				\dfrac{r_2}{-\frac{p_2}{2} + s_1 + r_1}
				\limpara \left(\dfrac{\D x_1}{\D x_2} \right)
				u(t,L(t)^+)
\end{displaymath}
As before we insert
\begin{displaymath}
				\limpara \left(\dfrac{\D x_1}{\D x_2} \right) =
				\sqrt{\frac{d_1}{d_2}}
\end{displaymath}
which leads us to the boundary condition 
\begin{equation}
				\label{eq:C_2_bc}
				u(t,L(t)^-) = 
				\dfrac{r_2}{-\frac{p_2}{2} + s_1 + r_1}
				\sqrt{\frac{d_1}{d_2}}
				u(t,L(t)^+)
\end{equation}

For the equation \eqref{eq:C_1_boundary} we get in a similar manner 
\begin{align*}
				 u(t, L(t) +  \frac{\D x_1}{2}) = &
				\frac{p_1}{2} u(t, L(t) +  \frac{\D x_1}{2}) +
				\O{\D x_1} \\ + & 
				(1 - s_2 - r_2)  u(t,L(t) + \frac{\D x_1}{2}) \\ + &
				r_1 \frac{\D x_2}{\D x_1} u(t,L(t) - \frac{\D x_2}{2}) +
				\O{\D t} \\
				\iff
				\left( - \frac{p_1}{2} + s_2 + r_2 \right)
				 u(t, L(t) +  \frac{\D x_1}{2}) = &
				r_1 \frac{\D x_2}{\D x_1} u(t,L(t) - \frac{\D x_2}{2}) +
				\O{\D x_1} +
				\O{\D t} \\
\end{align*}

Using the simultaneous parabolic limit as above we get another boundary condition
\begin{equation}
				\label{eq:C_1_bc}
				u(t,L(t)^-) = 
				\dfrac{-\frac{p_1}{2} + s_2 + r_2}{r_1}
				\sqrt{\frac{d_2}{d_1}}
				u(t,L(t)^+)
\end{equation}
which is in general different from expression \eqref{eq:C_2_bc} 

To arrive at a useful expression of the sort 
\begin{displaymath}
				u(t,L(t)^-) = 
				k_C
				u(t,L(t)^+)
\end{displaymath}
we demand that equation \eqref{eq:C_2_bc} and \eqref{eq:C_1_bc} are equal:
				\begin{equation}
								\label{eq:C_bothBCsequal}
								 r_1 r_2
								\demand
								\left(s_1 + r_1 - \frac{p_2}{2} \right)	
								\left(s_2 + r_2 - \frac{p_1}{2}  \right) 
								 .
				\end{equation}

Hence we are looking for a choice of parameters $r_i,s_i$ such that 
\begin{displaymath}
				0 \leq r_i + s_i \leq 1 
				\qquad
				0 \leq r_i \leq 1
				\qquad
				0 \leq s_i \leq 1
\end{displaymath}
and equation \eqref{eq:C_bothBCsequal} holds.


\subsubsection*{Finding the right parameters}
First note that we require $r_1 \cdot r_2$ to be positive, so both terms on the right side need to be positive as well.
The idea is to introduce a new parameter $\gamma \in (0,2)$ and assume 
\begin{displaymath}
				r_1 + r_2 = \gamma
				.
\end{displaymath}

Then insert this into equation \eqref{eq:C_bothBCsequal},  and show that the $s_i$ can be chosen from an interval such that $r_1$  can be anything in between $0$ and $\gamma$.
Compare $\gamma=1$ to Model A.

\clearpage
\section{Degree of civilization}
\label{sec:appendix:degree_civilization}

This is an alternative approach to section \ref{sec:evolution_boundary}.
We are looking for an ODE for $L(t)$,  derived through some mechanistic picture  just as the random walks used in the previous section 
\ref{sec:appendix:density_equation:model_A} - \ref{sec:appendix:density_equation:model_C}.

For modelling purposes we introduce a new function, the \emph{degree of civilization}:
				\begin{displaymath}
								w : [0,\infty) \times \R \mapsto [0,1] 
								.
				\end{displaymath}

The idea is that $w(t,x)$ measures to which extent the environment at $x$ has been adapted at time $t$, where $w(t,x) =1$ corresponds to the highest possible degree of civilization.
In the beaver picture, the biologist might count the number of dams established per metre and define anything with more than two dams per metre as $w(t,x)=1$, with one dam per metre as $w(t,x)=0.5$ and zero dams per metre as $w(t,x) =0$, etc..

First observe that under the assumption 
\begin{displaymath}
				\lim_{x \to \infty} w(t,x) = 0 
				\qquad
				\lim_{x \to -\infty} w(t,x) = 1
\end{displaymath}
the function 
\begin{math}
				t \mapsto -w(t,x)
\end{math}
is a probability distribution on $\R$.
We define 
\begin{displaymath}
				f(t,x) := - \partial_x w(t,x)
\end{displaymath}
as the probability density for the location of our boundary.
Then we connect the boundary $L(t)$ to the degree of civilization by setting 
\begin{displaymath}
				L (t) := <f(x)> =
				\int_{\R} x f(x) dx
\end{displaymath}
\begin{remark}
				If $w(t,x)$ is the Heaviside function, then $f(x)$ is a $\delta$-function and the boundary is where the step is:  $L(t)=0$.
\end{remark}

If we find a PDE for $w(t,x)$ of the type 
\begin{displaymath}
				w_t (t,x) = 
				h \left(u(t,x), u_x(t,x), L(t)\right)
\end{displaymath}
we get an ODE
\begin{align*}
				\dot{L(t)} = & 
				\dt{} \int_{\R} x f(t,x) dx =
				 \int_{\R} x f_t(t,x) dx   = 
				 - \int_{\R} x \partial_x h(u(t,x),u_x(t,x),L(t)) dx  \\ = &
				- \left[ x h(u(t,x),u_x(t,x),L(t))\right]_{-\infty}^{\infty} +
				\int_{\R} h(u(t,x),u_x(t,x),L(t)) dx \\  = &
				\int_{\R} h(u(t,x),u_x(t,x),L(t)) dx 
				.
\end{align*}
Here we made use of the fact that 
\begin{math}
				w_t(t,\pm \infty) = 0 
\end{math}
because we demand that only the environment close to the boundary changes.

In this setting, we have several approaches to come with the ODE for $L(t)$:
\begin{enumerate}[i)]
				\item Use our understanding of how the movement of individuals increases the degree of civilization. Derive a PDE for $w(t,x)$ through a random walk approach. 
								See section \ref{ssec:evolution_boundary:master_equation_w}.
				\item Do not use a mechanistic picture but some other argumentation to give a PDE for $w(t,x)$.
								Example: The degree of civilization grows logistically with a growth rate dependent on $u$ or $u_x$, i.e 
								\begin{align*}
												w_t(t,x) = & u(t,x) \cdot w(t,x) \left(1 - w(t,x) \right) \\
												w_t(t,x) = & u_x(t,x) \cdot w(t,x) \left(1 - w(t,x) \right) 
								\end{align*}
								Compare this to how settlers colonize a new colony.
								In the first few years, a lot of work is put into the basic infrastructure, lots of new streets, sewers and electricity lines are put up, the degree of civilization increases quickly.
								At a later stage more and more effort is put into conserving the existing intfrastructure, decoration such as village greens, recreation parks - the increase in living condition becomes infinitesimal (considering survival rates).

				\item  Derive a PDE for $f(t,x)$ through a random walk approach. 
								See L.'s original draft. 
								\meta{To translate notation: replace $q$ with $f$}
\end{enumerate}



\subsection*{Derive a master equation for $w(x)$}
\label{ssec:evolution_boundary:master_equation_w}

Now we turn to each model to derive a master equation for $w(x)$.
To this end we add some assumptions on how the individual movement helps to push the boundary forward into the unknown area $E_2
$.

\begin{enumerate}[i)]
				\item individuals which stay close to the boundary shift the boundary
				\item individuals which travel shift the boundary 
\end{enumerate}

\subsubsection*{Model B and Model C}
\label{sssec:evolution_boundary:master_equation_w:B}

Here we have four basic motions involving the two grid points which are the closest to the implicit boundary.
Individuals can either cross from $E_1$ into $E_2$ and vice versa or stay at their outposts.
\todo[Picture]{Scan a drawing which again shows the grid with crossing probabilities for Model B}\done

\includegraphics[page=1,angle=+90,width=\textwidth,clip,trim= 3cm 0cm 7cm 0cm]{pictures/boundarymotions.pdf}

We will use the four basic motions to derive a master equation. 
Define 
\begin{align*}
				&
				m_1 :=  q_1 \eta_1  &
				m_2 := & q_2 \eta_2\\
				&
				m_3 :=  (1 - q_2 - \frac{p_1}{2}) \eta _3  &
				m_4 :=  & (1 - q_1 - \frac{p_2}{2}) \eta _4 \\
\end{align*}
where the $\eta_i$ will later be chosen appropriately as normalizing constants.
\todo[Notation]{Swap $\alpha_1$ and $\alpha_2$. The mixing in the above formulas is unnatural.}\done

Here the interpretations are 
\begin{enumerate}[i)]
				\item[$m_1$] Cross from $L(t) - \frac{\D x_2}{2}$ into $L(t) + \frac{\D x_1}{2}$
				\item[$m_2$] vice versa
				\item[$m_3$] Stay at the point $L(t) + \frac{\D x_1}{2}$
				\item[$m_4$] Stay at the point $L(t) - \frac{\D x_2}{2}$
\end{enumerate}



\paragraph{Scenario B.1}
The simplest scenario would be that for each individual travelling from $L(t) - \frac{\D x_2}{2}$ to $L(t) + \frac{\D x_1}{2}$ the distance $\D x_0$ we shift the degree of civilization, i.e. the function $w(x)$, by a distance $\delta$
\begin{displaymath}
				\label{eq:master_equation_w:B_1}
				w(t + \D t, x ) =
				m_1  u(t,L(t)-\frac{\D x_2}{2} \D x_2 \cdot w(t,x - \delta)
\end{displaymath}


Now we try to calculate 
\begin{displaymath}
				\lim_{\D t \to 0}
				\dfrac{w(t+\D t) - w(t,x)}{\D t}
\end{displaymath}
Actually we should take the parabolic limit here, too. 
\meta{Nehme an $\delta = \kappa \D x_1$}

Finally we get 
\begin{displaymath}
				\dot{L(t)} = 
				\int_{\R} x f_t dx = \dots =
				2 d_1 \eta_2 \alpha_2 u(t,L(t) -) \kappa
\end{displaymath}

\subsubsection*{Model A}
\label{sssec:evolution_boundary:master_equation_w:A}

In principle there are three basic motions involving the boundary per time step.
Individuals can either jump from $E_1$ respectively $E_2$ onto the boundary grid point $L(t)$ or they already are there and just stay.

\includegraphics[page=1,angle=-90,width=\textwidth,clip,trim= 7cm 0cm 3cm 0cm]{pictures/randomwalks.pdf}

\begin{remark}[Problem for random walk approach]
				Here we run into a problem. 
				If we use a random walk approach to come up with an expression for $w(t +\D t,x)$, we will use the expression $u(t,L(t))$ in our equations.
				This is however not well-defined since our boundary condition derived in section \ref{sec:appendix:density_equation:model_A} implies that we cannot assign a single value to $u(t,L(t))$.
\end{remark}



\clearpage
\section{Heteroclinic orbits for the Fisher equation}
\label{sec:appendix:heteroclinic_orbits}
In the following we are going to assume $c > \sqrt{D_2r}$.
Consider the triangle $\Omega$ given by the three straight lines
\begin{align}
    S_1 := \{ (x,y) \in \R^2 : x = r , y < 0\} \\
                                S_2 := \{ (x,y) \in \R^2 : 0 < x < r , y = 0\} \\
                                S_3 := \{ (x,y) \in \R^2 :  y = \beta x \}
\end{align}

where $\beta$ is a negative real number yet to be chosen.

Let 
\begin{math}
    \Phi(z) = (\Phi_1(z),\Phi_2(z))
\end{math}
be the solution curve starting from $(r,0)$, leaving the saddle node through the repelling eigenvector $W_+$ enters the triangle $\Omega$.
We show, that
\begin{enumerate}[i)]
    \item The solution cannt leave the triangle through the sides,
    \item there cannot be a closed orbit within the triangle,
\end{enumerate}
and conclude that the solution curve must end up in $(0,0)$.
\todo[Existenz einer Lösungskurve ?]{Warum können wir davon ausgehen, dass es überhaubt eine Lösungskurve gibt, die die beiden Punkte verbindet ?}

\subparagraph{Concerning i)}
It is possible to show that the vector flow is pointing into the triangle along all of the three edges.
To that end we look at the scalar product between the outward normal $\eta_i$ and the vector flow $F(x,y)$ for all $(x,y) \in S_i, (i=1,2,3)$.

    \subparagraph{Case $(x,y) \in S_1$ }
        \begin{displaymath}
            <\eta_1, F(x,y)> =
                                                                                                < \vtwo{1}{0}, \vtwo{y}{ -\frac{c}{D_2} y} >
                                                                                                = y <0
        \end{displaymath}
    \subparagraph{Case $(x,y) \in S_2$ }
        \begin{displaymath}
            <\eta_2, F(x,y)> =
                                                                                                < \vtwo{0}{1}, \vtwo{0}{ -\frac{1}{D_2} x(r-x)} >
                                                                                                = -\frac{1}{D_2} x(r-x) <0
        \end{displaymath}
    \subparagraph{Case $(x,y) \in S_3$ }
        \begin{align}
            <\eta_3, F(x,y)> =
                                                                                                < \vtwo{\beta}{-1}, \vtwo{\beta}{ -\frac{c}{D_2} \beta x - \frac{1}{D_2}x(r=x)} >
                 \\  = x \left[\beta^2 + \frac{c}{D_2} \beta + \frac{r}{D_2} - \frac{x}{D_2} \right] <0
        \end{align}
We have to choose  $\beta$ such that
    \begin{align}
        & \beta^2 + \frac{c}{D_2} \beta + \frac{r}{D_2} - \frac{x}{D_2}  <0
        \qquad \forall 0 < x < r,
    \end{align}
which surely holds if 
    \begin{align*}
         & \beta^2 + \frac{c}{D_2} \beta + \frac{r}{D_2} <0 .
    \end{align*}

Let's calculate the zeros:
\begin{displaymath}
    \label{eq:beta_condition}
    \beta_{\pm} =
    \dfrac{-c \pm \sqrt{c^2 - 4 rD_2}}   {2D_2}
    < 0
    \qquad
    \implies \beta \in (\beta_-, \beta_+) = (\nu_-, \nu_+)
\end{displaymath}

If we choose
\begin{math}
    \beta \in (\beta_-, \beta_+) = (\nu_-, \nu_+),
\end{math}
we can conclude that the vector field is pointing inwards along all sides of the triangle and the solution curve cannot leave it.

\subparagraph{Concerning ii)}
We recall the negative Bendixon-Criterion as presented in \cite{kot_elements_2008}.
\todo[Quote einfügen]{}\done

\begin{theorem}[Negative Bendixon-Criterion]
    Consider the dynamical system
        \begin{displaymath}
            \dt{x_1} = F_1(x_1,x_2) \qquad
            \dt{x_2} = F_2(x_1,x_2) ,
        \end{displaymath}
    where $F_i \in C^1 ( \Omega )$ in a simply connected domain $\Omega \subseteq \R^2$.
    If
        \begin{math}
            \text{div}(F)   = \pd{F_1}{x_1}  + \pd{F_2}{x_2}
        \end{math}
    is of one sign in $\Omega$, then there can be no closed orbit within $\Omega$.
\end{theorem}

Here we have
    \begin{align*}
        \text{div}(F)  & = \pd{F_1}{x_1}  + \pd{F_2}{x_2} 
          = 0		  - \frac{c}{D_2}  < 0
          & \forall (x_1,x_2) \in \R^2 ,
    \end{align*}
 so there cannot be any closed orbit within the triangle defined by the three sides $S_1,S_2,S_3$.
\\

Since the solution curve cannot leave the triangle, we conclude that it must end up in the equilibrium point $(0,0)$.
\todo[Why not $(r,\beta r)$ as an exit point?]

 The solution curve approaches the origin through the eigenvector $V_+ = \vtwo{1}{ \nu_+} $ where
\begin{displaymath}
    \nu_{+} = \dfrac{ - c + \sqrt{ c^2 - 4D_2r}}{2D_2}
\end{displaymath}
